# 少儿青少年编程

#### 介绍
专门为做少儿编程的机构或个人提供在线解决方案.一个网站集编程,考试,课程,竞赛于一体

开源项目地址：https://gitee.com/zhaibaocai/python_52coding

如需了解和体验请联系 
**qq**   **1042629984**      
**wx**   **ziliao07** 

#### 软件架构
前端使用layui框架,后端使用python3.6以上 + Flask+mysql+redis


#### 安装教程

1.  安装python3.6以上,安装mysql,安装redis
2.  安装所需python包
3.  安装nginx

#### 使用说明

前端截图：
![输入图片说明](111.jpeg)
![输入图片说明](222.png)
后端截图：
1.  已经包含scratch,python,c++  1-4级考级题库(持续更新),包含信奥题库(持续更新),共3000多题库,持续更新中...
![输入图片说明](000.jpg)
2.  学生可以直接测试或考试
![输入图片说明](666.jpg)
![输入图片说明](999.jpg)
3.  在网页上可以直接进行 **scratch编程** , **python编程** (包含 _turtle绘图_ , _pgzero游戏编程_ ), **c++编程** 
![输入图片说明](555.jpg)
4. 可以在网站上直接使用NumPy、Pandas、Matplotlib等模块进行科学计算和绘图
![输入图片说明](%E7%A7%91%E5%AD%A6%E8%AE%A1%E7%AE%97%E7%AD%89.png)
5. 可以使用C++进行类似于turtle的绘图操作，降低C++学习门槛。
![输入图片说明](c++%E7%BB%98%E5%9B%BE.png)
6.  可以上传视频课程,pdf和md格式教案,每个课程都可以设置练习作业
![输入图片说明](111.jpg)
7. 课程可以上传视频，讲义，编辑课堂测试，添加实例。让学生学习更容易更方便
![输入图片说明](%E8%AF%BE%E7%A8%8B%E5%88%97%E8%A1%A8_%E8%AE%A9%E5%AD%A6%E7%94%9F%E5%AD%A6%E4%B9%A0%E6%9B%B4%E6%96%B9%E4%BE%BF.png)
8.  教师可以实时查看学生端编写的代码,教师可以实时掌握学生动态
![输入图片说明](222.jpg)
9.  有签到功能,方便教务管理
![输入图片说明](777.jpg)
![输入图片说明](888.jpg)
10.增加备课功能
![输入图片说明](%E5%A4%87%E8%AF%BE.png)
11.后台设置轮播图功能
![输入图片说明](%E8%BD%AE%E6%92%AD%E5%9B%BE%E8%AE%BE%E7%BD%AE.png)
12.后台可清理redis缓存
![输入图片说明](%E6%B8%85%E7%90%86%E7%BC%93%E5%AD%98.png)
13.后台设置注册发送验证码的邮箱
![输入图片说明](%E9%82%AE%E7%AE%B1%E6%B3%A8%E5%86%8C%E8%AE%BE%E7%BD%AE.png)
14.用户端教师和管理员登陆增加管理入口
![输入图片说明](%E6%95%99%E7%A8%8B%E5%92%8C%E7%AE%A1%E7%90%86%E5%91%98%E7%99%BB%E9%99%86%E7%94%A8%E6%88%B7%E9%A1%B5%E6%96%B0%E5%A2%9E%E7%AE%A1%E7%90%86%E5%85%A5%E5%8F%A3.png)
15.可以组织竞赛
![输入图片说明](111.png)
![输入图片说明](222.png)
16.可以直接在浏览器上使用pgzero编辑游戏，并且可共享使用scratch素材
![输入图片说明](11123.png)

演示地址：https://52coding.net
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

